from . import create_app

app = create_app(config='dev')
app.run('0.0.0.0', port=8080, debug=True)
