import os


class Config:
    """Base configuration."""

    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    DEFAULT_LOCALE = 'en'


class ProdConfig(Config):
    ENV = 'prod'
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    ENV = 'dev'
    DEBUG = True

    #
    # SQLAlchemy
    #
    SQLALCHEMY_DATABASE_URI = 'sqlite:///../dev.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class TestConfig(DevConfig):
    ENV = 'test'
    TESTING = True
    DEBUG = True

    #
    # SQLAlchemy
    #
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_ECHO = False  # Prints all SQL to stdout
