from curb.database import Model, db, Column, UuidPrimaryKey

from curb.extensions import marshmallow as ma


class Instructor(Model, UuidPrimaryKey):
    __tablename__ = "instructors"

    first_name = Column(db.Unicode(255), nullable=False)
    last_name = Column(db.Unicode(255), nullable=False)


class InstructorSchema(ma.Schema):
    class Meta:
        fields = ("first_name", "last_name")


instructor_schema = InstructorSchema()
instructors_schema = InstructorSchema(many=True)
