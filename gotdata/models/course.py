from curb.database import Model, db, Column, UuidPrimaryKey

from curb.extensions import marshmallow as ma


class Course(Model, UuidPrimaryKey):
    __tablename__ = "courses"

    title = Column(db.Unicode(255), nullable=False, unique=True)


class CourseSchema(ma.Schema):
    class Meta:
        fields = ("title",)


course_schema = CourseSchema()
courses_schema = CourseSchema(many=True)
