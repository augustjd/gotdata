from .course import Course, course_schema, courses_schema
from .instructor import Instructor, instructor_schema, instructors_schema
from .section import Section, section_schema, sections_schema


assert(Course)
assert(course_schema)
assert(courses_schema)

assert(Instructor)
assert(instructor_schema)
assert(instructors_schema)

assert(Section)
assert(section_schema)
assert(sections_schema)
