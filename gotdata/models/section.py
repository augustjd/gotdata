from curb.database import Model, db, Column, UuidPrimaryKey, reference_col

from curb.extensions import marshmallow as ma

from .instructor import InstructorSchema


class Section(Model, UuidPrimaryKey):
    __tablename__ = "courses"

    index = Column(db.Integer, nullable=False)
    call_number = Column(db.Integer, nullable=False)
    title = Column(db.Unicode(255), nullable=True)

    instructor_id = reference_col("instructors", nullable=False)
    instructor = db.relationship("Instructor", uselist=False, backref="sections")

    course_id = reference_col("courses", nullable=False)
    course = db.relationship("Course", uselist=False, backref="sections")


class SectionSchema(ma.Schema):
    class Meta:
        fields = ("index", "title", "instructor")

    instructor = ma.Nested(InstructorSchema)


section_schema = SectionSchema()
sections_schema = SectionSchema(many=True)
